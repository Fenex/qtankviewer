#pragma once
#include "xmlnode.h"


struct DataDescriptor
{
    uint32_t end : 28;
    uint32_t type : 4;
};


#pragma pack(push)
#pragma pack(1)

struct ElementDescriptor {
    uint16_t nameIndex;
    DataDescriptor descr;
};

#pragma pack(pop)


class PackedSection
{
private:
    QStringList dict;
    QBuffer *buffer;

    QString readString(const size_t lengthInBytes);
    QString readNumber(const size_t lengthInBytes);
    QString readBoolean(const size_t lengthInBytes);
    QString readBase64(const size_t lengthInBytes);
    QStringList readFloats(const size_t lengthInBytes);

    void readDictionary();
    QString readASCIIZ();
    void readElement(XMLNodePtr node);
    size_t readData(XMLNodePtr node, const size_t offset, const ElementDescriptor &el_descr);

    template <typename T>
    T read();

public:
    PackedSection(QByteArray &data, XMLNodePtr node);
};
