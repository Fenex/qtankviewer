#include "resmgr.h"
#include "ttvfs.h"
#include "ttvfs_zip.h"


ttvfs::Root ResMgr::vfs;


void ResMgr::init()
{
    qDebug() << Q_FUNC_INFO;
    vfs.AddLoader(new ttvfs::DiskLoader);
    vfs.AddArchiveLoader(new ttvfs::VFSZipArchiveLoader);
}

void ResMgr::fini()
{
    qDebug() << Q_FUNC_INFO;
}

void ResMgr::mount_pkgs(const QStringList &paths)
{
    QElapsedTimer timer;
    timer.start();

    for (QString path : paths) {
        std::string s = path.toStdString();
        vfs.AddArchive(s.c_str());
        vfs.Mount(s.c_str(), "");
    }

    qDebug() << __FUNCTION__ << "time:" << timer.elapsed();
}

void ResMgr::mount_dirs(const QStringList &paths)
{
    QElapsedTimer timer;
    timer.start();

    for (QString path : paths) {
        std::string s = path.toStdString();
        vfs.Mount(s.c_str(), "");
    }

    qDebug() << __FUNCTION__ << "time:" << timer.elapsed();
}

XMLSectionPtr ResMgr::open_section(const QString &filename)
{
    //QElapsedTimer timer;
    //timer.start();

    QByteArray f = read_file(filename);
    if (f.isNull()) {
        return nullptr;
    }
    XMLSectionPtr ptr = XMLSectionPtr::create(f);

    //qDebug() << __FUNCTION__ << "time:" << timer.elapsed();

    return ptr;
}

QByteArray ResMgr::open_binary(const QString &filename, const QString &name)
{
    std::string s = filename.toStdString();
    ttvfs::CountedPtr<ttvfs::File> vf = vfs.GetFile(s.c_str());

    if (vf && vf->open("rb")) {
        uint32_t magic;
        vf->read(&magic, 4);

        if (magic != 0x42a14e65) {
            qWarning() << "Wrong magic:" << magic;
            vf->close();
            return nullptr;
        }

        vf->seek(vf->size() - 4, SEEK_SET);

        uint32_t indexLen;
        vf->read(&indexLen, 4);

        int offset = indexLen + 4;
        vf->seek(vf->size() - offset, SEEK_SET);

        uint32_t oldDataLen = 4;

        while (vf->getpos() < vf->size() - 4)
        {
            uint32_t entryDataLen;
            vf->read(&entryDataLen, 4);

            // skip
            vf->seek(16, SEEK_CUR);

            uint32_t entryNameLen;
            vf->read(&entryNameLen, 4);

            QByteArray entryStr;
            entryStr.resize(entryNameLen);
            vf->read(entryStr.data(), entryNameLen);

            if (entryNameLen % 4 > 0)
                vf->seek(4 - (entryNameLen % 4), SEEK_CUR);

            if (entryStr == name.toLatin1()) {
                vf->seek(oldDataLen, SEEK_SET);
                QByteArray ba;
                ba.resize(entryDataLen);
                vf->read(ba.data(), entryDataLen);
                vf->close();
                return ba;
            }

            oldDataLen += (entryDataLen + 3) & (~3L);
        }

        vf->close();
    }
    return nullptr;
}

QByteArray ResMgr::read_file(const QString &filename)
{
    std::string s = filename.toStdString();
    return read_file(s.c_str());
}

QByteArray ResMgr::read_file(const char *filename)
{
    ttvfs::CountedPtr<ttvfs::File> vf = vfs.GetFile(filename);
    if (vf && vf->open("rb")) {
        QByteArray ba;
        ba.resize(vf->size());
        vf->read(ba.data(), vf->size());
        vf->close();
        return ba;
    }
    return nullptr;
}
