#pragma once
#include "xmlsection.h"


namespace ttvfs {
    class Root;
}


class ResMgr
{
private:
    static ttvfs::Root vfs;

public:
    static void init();
    static void fini();

    static void mount_pkgs(const QStringList &paths);
    static void mount_dirs(const QStringList &paths);

    static XMLSectionPtr open_section(const QString &filename);
    static QByteArray open_binary(const QString &filename, const QString &name);

    static QByteArray read_file(const QString &filename);
    static QByteArray read_file(const char *filename);
};
