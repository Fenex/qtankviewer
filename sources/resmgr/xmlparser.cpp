#include "xmlparser.h"


XMLParser::XMLParser(XMLNodePtr root_node) : QXmlDefaultHandler()
{
    m_current_node = root_node;
    level = 0;
}

XMLParser::~XMLParser()
{
    //qDebug() << Q_FUNC_INFO;
}

bool XMLParser::startElement(const QString &, const QString &localName, const QString &, const QXmlAttributes &)
{
    if (level > 0) {
        XMLNodePtr new_node = XMLNodePtr::create();
        new_node->prev = m_current_node;
        m_current_node.data()->childrens.insert(localName, new_node);
        m_current_node = new_node;
    }
    level++;
    return true;
}

bool XMLParser::characters(const QString &ch)
{
    m_current_node.data()->value = ch.trimmed();
    return true;
}

bool XMLParser::endElement(const QString &, const QString &, const QString &)
{
    if (level>0)
        m_current_node = m_current_node.data()->prev;
    level--;
    return true;
}
