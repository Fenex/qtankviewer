#include "packedsection.h"


PackedSection::PackedSection(QByteArray &data, XMLNodePtr node)
{
    buffer = new QBuffer(&data);
    if (!buffer->open(QBuffer::ReadOnly))
        return;

    buffer->skip(5);

    readDictionary();

    readElement(node);

    buffer->close();
    delete buffer;
}

QString PackedSection::readASCIIZ()
{
    QString str;
    char c;
    while (true) {
        buffer->getChar(&c);
        if (c == '\0')
            return str;
        str.append(c);
    }
}

void PackedSection::readDictionary()
{
    QString str;
    while ((str = readASCIIZ()) != "") {
        dict.append(str);
    }
}

template <typename T>
T PackedSection::read()
{
    T data;
    buffer->read((char*)&data, sizeof(T));
    return data;
}

QString PackedSection::readNumber(const size_t lengthInBytes)
{
    QString str;
    switch (lengthInBytes)
    {
    case 0:
        str = "0";
        break;
    case 1:
    {
        int8_t i8 = read<int8_t>();
        str = QString::number(i8);
        break;
    }
    case 2:
    {
        int16_t i16 = read<int16_t>();
        str = QString::number(i16);
        break;
    }
    case 4:
    {
        int32_t i32 = read<int32_t>();
        str = QString::number(i32);
        break;
    }
    case 8:
    {
        int64_t i64 = read<int64_t>();
        str = QString::number(i64);
        break;
    }
    default:
        Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported int size!");
    }
    return str;
}

QString PackedSection::readString(const size_t lengthInBytes)
{
    QString res = "";
    if (lengthInBytes > 0) {
        res = buffer->read(lengthInBytes);
    }
    return res;
}

size_t PackedSection::readData(XMLNodePtr node, const size_t offset, const ElementDescriptor &el_descr)
{
    size_t lengthInBytes = el_descr.descr.end - offset;

    switch (el_descr.descr.type)
    {
    case 0:
    {
        readElement(node);
        break;
    }
    case 1:
    {
        node->value = readString(lengthInBytes);
        break;
    }
    case 2:
    {
        node->value = readNumber(lengthInBytes);
        break;
    }
    case 3:
    {
        QStringList float_list = readFloats(lengthInBytes);
        if (lengthInBytes != 48)
        {
            node->value = float_list.join(" ");
        }
        else if (lengthInBytes == 48)
        {
            auto transform_node = XMLNodePtr::create();
            node->childrens.insert("transform", transform_node);

            auto new_node = XMLNodePtr::create();
            new_node->value = float_list.mid(0, 3).join(" ");
            transform_node->childrens.insert("row0", new_node);

            new_node = XMLNodePtr::create();
            new_node->value = float_list.mid(3, 3).join(" ");
            transform_node->childrens.insert("row1", new_node);

            new_node = XMLNodePtr::create();
            new_node->value = float_list.mid(6, 3).join(" ");
            transform_node->childrens.insert("row2", new_node);

            new_node = XMLNodePtr::create();
            new_node->value = float_list.mid(9, 3).join(" ");
            transform_node->childrens.insert("row3", new_node);
        }
        break;
    }
    case 4:
    {
        node->value = readBoolean(lengthInBytes);
        break;
    }
    case 5:
    {
        node->value = readBase64(lengthInBytes);
        break;
    }
    case 6:
    {
        buffer->skip(lengthInBytes);
        qWarning() << "Unsupported section type!";
        break;
    }
    default:
        qWarning() << "Unsupported section type!";
        Q_ASSERT(false);
    }
    return el_descr.descr.end;
}

QString PackedSection::readBoolean(const size_t lengthInBytes)
{
    if (lengthInBytes > 0) {
        buffer->skip(lengthInBytes);
        return "true";
    }
    return "false";
}

QStringList PackedSection::readFloats(const size_t lengthInBytes)
{
    Q_ASSERT(lengthInBytes % sizeof(float) == 0);

    QStringList float_list;

    size_t nf = lengthInBytes/sizeof(float);

    for (size_t i = 0; i<nf; i++) {
        float f = read<float>();
        float_list.append(QString::number(f, 'f', 6));
    }
    return float_list;
}

QString PackedSection::readBase64(const size_t lengthInBytes)
{
    return buffer->read(lengthInBytes).toBase64();
}

void PackedSection::readElement(XMLNodePtr node)
{
    ElementDescriptor first_element;
    buffer->read((char*)&first_element, sizeof(ElementDescriptor));

    ElementDescriptor *children = new ElementDescriptor[first_element.nameIndex];
    buffer->read((char*)children, first_element.nameIndex*sizeof(ElementDescriptor));

    size_t offset = readData(node, 0, first_element);

    for (uint16_t i = 0; i<first_element.nameIndex; i++) {
        XMLNodePtr new_node = XMLNodePtr::create();
        offset = readData(new_node, offset, children[i]);
        QString name = dict[children[i].nameIndex];
        node->childrens.insert(name, new_node);
    }

    delete[] children;
}
