#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "preferenceswindow.h"
#include "tanksdatabase.h"
#include "utils/wotmodel.h"
#include <QWidget>
#include <Qt3DExtras/Qt3DWindow>
#include <Qt3DExtras/QOrbitCameraController>
#include <Qt3DExtras/QFirstPersonCameraController>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DRender/QPointLight>
#include <Qt3DCore/QTransform>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupData();

    auto view = new Qt3DExtras::Qt3DWindow;
    rootEntity = new Qt3DCore::QEntity;

    auto cameraController = new Qt3DExtras::QOrbitCameraController(rootEntity);
    cameraController->setLinearSpeed( 10.0f );
    cameraController->setLookSpeed( 60.0f );
    cameraController->setCamera(view->camera());
    cameraController->camera()->setPosition(QVector3D(0, 0, 10.0f));
    cameraController->camera()->setViewCenter(QVector3D(0,0,0));


    auto lightEntity = new Qt3DCore::QEntity(rootEntity);
    auto light = new Qt3DRender::QPointLight(lightEntity);
    light->setColor("white");
    light->setIntensity(1);
    lightEntity->addComponent(light);
    auto lightTransform = new Qt3DCore::QTransform(lightEntity);
    lightTransform->setTranslation(cameraController->camera()->position());
    lightEntity->addComponent(lightTransform);

    auto widget = QWidget::createWindowContainer(view);
    ui->centralwidget->layout()->addWidget(widget);

    tankEntity = new Qt3DCore::QEntity(rootEntity);

    view->setRootEntity(rootEntity);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupData()
{
    ui->nationsComboBox->addItems(
                TanksDatabase::translatedNations);

    ui->typesComboBox->addItems(
                TanksDatabase::translatedTypes);

    ui->levelsComboBox->addItems(
                TanksDatabase::translatedLevels);

    refreshByFilters();
}

void MainWindow::refreshByFilters()
{
    ui->tankListWidget->clear();

    auto list = TanksDatabase::getTranslatedTankListByParams(
                nationIdx, tagsIdx, levelIdx);

    for (auto it : list) {
        QListWidgetItem *witem = new QListWidgetItem(it.first);
        witem->setData(Qt::UserRole, it.second);
        ui->tankListWidget->addItem(witem);
    }
}

void MainWindow::on_actionPreferences_triggered()
{
    PreferencesWindow pw(this);
    pw.exec();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_nationsComboBox_currentIndexChanged(int index)
{
    if (nationIdx == index)
        return;

    nationIdx = index;
    refreshByFilters();
}

void MainWindow::on_typesComboBox_currentIndexChanged(int index)
{
    if (tagsIdx == index)
        return;

    tagsIdx = index;
    refreshByFilters();
}

void MainWindow::on_levelsComboBox_currentIndexChanged(int index)
{
    if (levelIdx == index)
        return;

    levelIdx = index;
    refreshByFilters();
}

void MainWindow::on_tankListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    int tank_db_id = item->data(Qt::UserRole).toInt();

    vInfo = TanksDatabase::loadVechInfo(tank_db_id);

    if (vInfo.isNull())
        return;

    ui->turret_comboBox->clear();
    ui->turret_comboBox->addItems(vInfo->turrets.keys());

    load_models();
}

void MainWindow::on_turret_comboBox_currentIndexChanged(int index)
{
    if (index == -1)
        return;

    ui->gun_comboBox->clear();
    ui->gun_comboBox->addItems(vInfo->turrets.values().at(index).guns.keys());
}

static void recurse_clear(Qt3DCore::QEntity *entity)
{
    if (entity == nullptr)
        return;

    for (auto child : entity->children()) {
        recurse_clear(qobject_cast<Qt3DCore::QEntity *>(child));
    }

    delete entity;
}

void MainWindow::load_models()
{
    recurse_clear(tankEntity);
    tankEntity = new Qt3DCore::QEntity(rootEntity);

    auto chassis = new WoTModel(
                vInfo->chassis.undamaged_model, QVector3D(0, 0, 0), tankEntity);
    chassis->setObjectName("chassis");

    auto hull = new WoTModel(
                vInfo->hull.undamaged_model, vInfo->chassis.hullPosition, chassis);
    hull->setObjectName("hull");

    int turretIdx = ui->turret_comboBox->currentIndex();
    auto turret = new WoTModel(
                vInfo->turrets.values()[turretIdx].undamaged_model,
                vInfo->hull.turretPositions, hull);
    turret->setObjectName("turret");

    int gunIdx = ui->gun_comboBox->currentIndex();
    auto gun = new WoTModel(
                vInfo->turrets.values()[turretIdx].guns.values()[gunIdx].undamaged_model,
                vInfo->turrets.values()[turretIdx].gunPosition, turret);
    gun->setObjectName("gun");
}

void MainWindow::on_pushButton_clicked()
{
    load_models();
}
