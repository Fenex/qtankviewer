#include "preferenceswindow.h"
#include "ui_preferenceswindow.h"
#include "app.h"
#include <QFileDialog>
#include <QMessageBox>


PreferencesWindow::PreferencesWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreferencesWindow)
{
    ui->setupUi(this);
    ui->lineEditWotPath->setText(TankViewerApp::wotPath());
    ui->lineEditModPath->setText(TankViewerApp::modPath());
}

PreferencesWindow::~PreferencesWindow()
{
    delete ui;
}

void PreferencesWindow::on_buttonBox_accepted()
{
    const QString wotPath = ui->lineEditWotPath->text();
    const QString modPath = ui->lineEditModPath->text();

    QFile wotExePath(wotPath + "/WorldOfTanks.exe");
    if (wotExePath.exists()) {
        TankViewerApp::setWotPath(wotPath);
        TankViewerApp::setModPath(modPath);
    }
    else {
        QMessageBox::warning(this, "", tr("World of Tanks path does not appear to be valid.\n"
                                          "Pleas check Preferences again."));
    }

}

void PreferencesWindow::on_btnWotPath_clicked()
{
    QString wotPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "", QFileDialog::ShowDirsOnly);
    if (!wotPath.isEmpty()) {
        ui->lineEditWotPath->setText(wotPath);
    }
}

void PreferencesWindow::on_btnModPath_clicked()
{
    QString modPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "", QFileDialog::ShowDirsOnly);
    if (!modPath.isEmpty()) {
        ui->lineEditModPath->setText(modPath);
    }
}
