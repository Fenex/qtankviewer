TEMPLATE = app
QT += widgets core xml sql 3dcore 3drender 3dinput 3dextras


contains(QMAKE_HOST.arch, x86_64):ARCH_SUFFIX = x64
else:ARCH_SUFFIX = x32

TARGET = TankViewer_$$ARCH_SUFFIX
DESTDIR = $$PWD/../build/bin_$$ARCH_SUFFIX

CONFIG(debug, debug|release) {
    LIB_SUFFIX = d
}

include($$PWD/resmgr/resmgr.pri)


SOURCES += \
    app.cpp \
    main.cpp \
    mainwindow.cpp \
    tanksdatabase.cpp \
    preferenceswindow.cpp \
    utils/localizationutil.cpp \
    utils/mo_file.cpp \
    utils/vechicleinfo.cpp \
    utils/wotmodel.cpp \
    utils/wotmesh.cpp \
    utils/vertextypes.cpp

HEADERS += \
    common.h \
    app.h \
    mainwindow.h \
    preferenceswindow.h \
    tanksdatabase.h \
    utils/localizationutil.h \
    utils/mo_file.h \
    utils/vechicleinfo.h \
    utils/wotmodel.h \
    utils/wotmesh.h \
    utils/vertextypes.h

FORMS += \
    mainwindow.ui \
    preferenceswindow.ui

DISTFILES += \
    ../build/data/resources.xml
