#pragma once
#include "resmgr/xmlsection.h"
#include "wotmesh.h"
#include <Qt3DCore/QEntity>


class WoTModel : public Qt3DCore::QEntity
{
private:
    XMLSectionPtr visual;

    void load_mesh(const QString &primitives_path,
                   const QString &vres_name,
                   const QString &pres_name);

public:
    WoTModel(const QString &modelPath,
             const QVector3D &pos,
             QEntity *tankEntity);
};
