#include "vechicleinfo.h"
#include "resmgr/resmgr.h"
#include "utils/localizationutil.h"


VechicleInfo::VechicleInfo(const QString &path)
{
    XMLSectionPtr sec = ResMgr::open_section(path);
    if (sec.isNull()) {
        return;
    }

    LocalizationUtil l10n;

    {
        // chassis
        auto chassis_sec = sec->openSection("chassis")->values().at(0);
        chassis.undamaged_model = chassis_sec->readString("models/undamaged");
        chassis.destroyed_model = chassis_sec->readString("models/destroyed");
        chassis.collision_model = chassis_sec->readString("hitTester/collisionModelClient");
        chassis.hullPosition = chassis_sec->readVector3("hullPosition");
    }

    {
        // hull
        hull.undamaged_model = sec->readString("hull/models/undamaged");
        hull.destroyed_model = sec->readString("hull/models/destroyed");
        hull.collision_model = sec->readString("hitTester/collisionModelClient");
        hull.turretPositions = sec->readVector3("hull/turretPositions/turret");
    }

    {
        // turrets0
        auto turrets0_sec = sec->openSection("turrets0");

        for (auto turret_sec : turrets0_sec->values()) {
            QString turret_str = turret_sec->readString("userString");

            TurretInfo turretInfo;

            turretInfo.undamaged_model = turret_sec->readString("models/undamaged");
            turretInfo.destroyed_model = turret_sec->readString("models/destroyed");
            turretInfo.collision_model = turret_sec->readString("hitTester/collisionModelClient");
            turretInfo.gunPosition = turret_sec->readVector3("gunPosition");

            // very dirty hack
            QString turret_str_split0;

            if (turret_str.indexOf(':') != -1)
                turret_str_split0 = turret_str.split(':')[0] + ':';

            auto guns = turret_sec->openSection("guns")->items();

            for (auto gun : guns) {
                // guns
                GunInfo gunInfo;
                gunInfo.undamaged_model = gun.second->readString("models/undamaged");
                gunInfo.destroyed_model = gun.second->readString("models/undamaged");
                gunInfo.collision_model = gun.second->readString("hitTester/collisionModelClient");
                QString gun_str = turret_str_split0 + gun.first;
                turretInfo.guns.insert(l10n.tr(gun_str), gunInfo);
            }

            turrets.insert(l10n.tr(turret_str), turretInfo);
        }
    }
}
