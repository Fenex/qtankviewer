#pragma once
#include "common.h"


class ChassisInfo
{
public:
    QString undamaged_model;
    QString destroyed_model;
    QString collision_model;
    QVector3D hullPosition;
};


class HullInfo
{
public:
    QString undamaged_model;
    QString destroyed_model;
    QString collision_model;
    QVector3D turretPositions;
};


class GunInfo
{
public:
    QString undamaged_model;
    QString destroyed_model;
    QString collision_model;
};


class TurretInfo
{
public:
    QString undamaged_model;
    QString destroyed_model;
    QString collision_model;
    QVector3D gunPosition;
    QMap<QString, GunInfo> guns;
};


class VechicleInfo
{
public:
    VechicleInfo(const QString &path);
    ChassisInfo chassis;
    HullInfo hull;
    QMultiMap<QString, TurretInfo> turrets;
};


typedef QSharedPointer<VechicleInfo> VechicleInfoPtr;
