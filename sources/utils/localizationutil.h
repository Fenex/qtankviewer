#pragma once
#include "mo_file.h"


class LocalizationUtil
{
private:
    QHash<QString, Localization_Text_Ptr> cache;

public:
    LocalizationUtil();
    QString tr(const QString &s);
};
