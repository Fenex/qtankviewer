#include "mo_file.h"
#include <cstring>


#define HASHWORDBITS 32

inline unsigned long hashpjw(const char *str_param) {

   unsigned long hval = 0;
   unsigned long g;
   const char *s = str_param;
 
   while (*s) {
       hval <<= 4;
       hval += (unsigned char) *s++;
       g = hval & ((unsigned long int) 0xf << (HASHWORDBITS - 4));
       if (g != 0) {
           hval ^= g >> (HASHWORDBITS - 8);
           hval ^= g;
       }
   }

   return hval;
}

inline unsigned long my_swap4(unsigned long result) {
    unsigned long c0 = (result >> 0) & 0xff;
    unsigned long c1 = (result >> 8) & 0xff;
    unsigned long c2 = (result >> 16) & 0xff;
    unsigned long c3 = (result >> 24) & 0xff;

    return (c0 << 24) | (c1 << 16) | (c2 << 8) | c3;
}

inline int Localization_Text::read4_from_offset(int offset)
{
    unsigned long *ptr = (unsigned long *)(((char *)this->mo_data) + offset);
    
    if (this->reversed) {
        return my_swap4(*ptr);
    } else {
        return *ptr;
    }
}

inline char *Localization_Text::get_source_string(int index)
{
    int addr_offset = this->original_table_offset + 8 * index + 4;
    int string_offset = read4_from_offset(addr_offset);

    char *t = ((char *)this->mo_data) + string_offset;
    return t;
}

inline char *Localization_Text::get_translated_string(int index)
{
    int addr_offset = this->translated_table_offset + 8 * index + 4;
    int string_offset = read4_from_offset(addr_offset);

    char *t = ((char *)this->mo_data) + string_offset;
    return t;
}

bool Localization_Text::label_matches(char *s, int index)
{
    char *t = get_source_string(index);
    if (strcmp(s, t) == 0) return true;
    
    return false;
}

inline int Localization_Text::get_target_index(char *s)
{
    unsigned long V = hashpjw(s);
    int S = this->hash_num_entries;

    int hash_cursor = V % S;
    int orig_hash_cursor = hash_cursor;
    int increment = 1 + (V % (S - 2));

    while (1) {
        unsigned int index = read4_from_offset(this->hash_offset + 4 * hash_cursor);
        if (index == 0) break;

        index--;
		
        if (label_matches(s, index)) return index;
        
        hash_cursor += increment;
        hash_cursor %= S;

        if (hash_cursor == orig_hash_cursor) break;
    }

    return -1;
}

Localization_Text::Localization_Text(const QByteArray &data) {
    mo_ba = data;
    mo_data = mo_ba.data();

    num_strings = 0;
    original_table_offset = 0;
    translated_table_offset = 0;
    hash_num_entries = 0;
    hash_offset = 0;

    if (mo_ba.size() < 24) {
        abort();
        return;
    }

    unsigned long *long_ptr = (unsigned long *)mo_data;

    const unsigned long TARGET_MAGIC = 0x950412DE;
    const unsigned long TARGET_MAGIC_REVERSED = 0xDE120495;
    unsigned long magic = long_ptr[0];

    if (magic == TARGET_MAGIC) {
        reversed = 0;
    } else if (magic == TARGET_MAGIC_REVERSED) {
        reversed = 1;
    } else {
        return;
    }

    num_strings = read4_from_offset(8);
    original_table_offset = read4_from_offset(12);
    translated_table_offset = read4_from_offset(16);
    hash_num_entries = read4_from_offset(20);
    hash_offset = read4_from_offset(24);

    if (hash_num_entries == 0) {
        return;
    }
}

char *Localization_Text::text_lookup(const char *s) {
    if (!this->mo_data) return (char*)s;

    int target_index = get_target_index((char*)s);
    if (target_index == -1) return (char*)s;

    return get_translated_string(target_index);
}
