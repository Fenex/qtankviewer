#include "vertextypes.h"


Q_STATIC_ASSERT(sizeof(set3_xyznuviiiwwtbpc_s) == 40);
Q_STATIC_ASSERT(sizeof(set3_xyznuvtbpc_s) == 32);
Q_STATIC_ASSERT(sizeof(set3_xyznuvpc_s) == 24);

Q_STATIC_ASSERT(sizeof(xyznuviiiwwtb_s) == 37);
Q_STATIC_ASSERT(sizeof(xyznuvtb_s) == 32);
Q_STATIC_ASSERT(sizeof(xyznuv_s) == 32);


float* unpack_normal_old(uint32_t packed)
{
    float *vec3 = new float[3];

    uint32_t pky = (packed >> 22) & 0x1FF;
    uint32_t pkz = (packed >> 11) & 0x3FF;
    uint32_t pkx = (packed      ) & 0x3FF;

    vec3[0] = pkx/1023.0;
    if (pkx & (1<<10)) {
        vec3[0] = -vec3[0];
    }

    vec3[1] = pky/511.0;
    if (pkx & (1<<9)) {
        vec3[1] = -vec3[1];
    }

    vec3[2] = pkz/1023.0;
    if (pkx & (1<<10)) {
        vec3[2] = -vec3[2];
    }

    return vec3;
}


float* unpack_normal_new(uint32_t packed)
{
    float *vec3 = new float[3];

    uint32_t pkz = ((packed >> 16) & 0xFF) ^ 0xFF;
    uint32_t pky = ((packed >> 8 ) & 0xFF) ^ 0xFF;
    uint32_t pkx = ((packed      ) & 0xFF) ^ 0xFF;

    if (pkx > 0x7f) {
        vec3[0] = -float(pkx & 0x7f)/127.0f;
    } else {
        vec3[0] = float(pkx ^ 0x7f)/127.0f;
    }

    if (pky > 0x7f) {
        vec3[1] = -float(pky & 0x7f)/127.0f;
    } else {
        vec3[1] = float(pky ^ 0x7f)/127.0f;
    }

    if (pkz > 0x7f) {
        vec3[2] = -float(pkz & 0x7f)/127.0f;
    } else {
        vec3[2] = float(pkz ^ 0x7f)/127.0f;
    }

    return vec3;
}
